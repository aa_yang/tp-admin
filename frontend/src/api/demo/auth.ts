import { defHttp } from '/@/utils/http/axios';

enum Api {
  AdminList = '/auth.admin/index',
  AdminAdd = '/auth.admin/add',
  AdminEdit = '/auth.admin/edit',
  AdminDel = '/auth.admin/del',
  GroupList = '/auth.group/index',
  GroupAdd = '/auth.group/add',
  GroupEdit = '/auth.group/edit',
  GroupDel = '/auth.group/del',
  RuleList = '/auth.rule/index',
  RuleAdd = '/auth.rule/add',
  RuleEdit = '/auth.rule/edit',
  RuleDel = '/auth.rule/del',
  LogList = '/auth.log/index',
  LogDel = '/auth.log/del',
  AjaxGroup = '/ajax/getGroupList',
}

export const getAdminList = (params?: any) => defHttp.get<any>({ url: Api.AdminList, params });

export const AdminAdd = (params?: any) => defHttp.post<any[]>({ url: Api.AdminAdd, params });

export const AdminEdit = (params?: any) => defHttp.put<any[]>({ url: Api.AdminEdit, params });

export const AdminDel = (params?: any) => defHttp.delete<any[]>({ url: Api.AdminDel, params });

export const getGroupList = (params?: any) => defHttp.get<any>({ url: Api.GroupList, params });

export const GroupAdd = (params?: any) => defHttp.post<any[]>({ url: Api.GroupAdd, params });

export const GroupEdit = (params?: any) => defHttp.put<any[]>({ url: Api.GroupEdit, params });

export const GroupDel = (params?: any) => defHttp.delete<any[]>({ url: Api.GroupDel, params });

export const getAuleList = (params?: any) => defHttp.get<any>({ url: Api.RuleList, params });

export const RuleAdd = (params?: any) => defHttp.post<any[]>({ url: Api.RuleAdd, params });

export const RuleEdit = (params?: any) => defHttp.put<any[]>({ url: Api.RuleEdit, params });

export const RuleDel = (params?: any) => defHttp.delete<any[]>({ url: Api.RuleDel, params });

export const getLogList = (params?: any) => defHttp.get<any>({ url: Api.LogList, params });

export const LogDel = (params?: any) => defHttp.delete<any[]>({ url: Api.LogDel, params });

export const ajaxGroupList = (params?: any) => defHttp.get<any>({ url: Api.AjaxGroup, params });
