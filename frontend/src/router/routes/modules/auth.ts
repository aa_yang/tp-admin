import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const auth: AppRouteModule = {
  path: '/auth',
  name: 'Auth',
  component: LAYOUT,
  redirect: '/auth/admin',
  meta: {
    orderNo: 2000,
    icon: 'ion:settings-outline',
    title: t('routes.demo.system.moduleName'),
  },
  children: [
    {
      path: 'admin',
      name: 'AdminManagement',
      meta: {
        title: t('routes.demo.system.account'),
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/auth/admin/index.vue'),
    },
    {
      path: 'group',
      name: 'GroupManagement',
      meta: {
        title: t('routes.demo.system.role'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/auth/group/index.vue'),
    },
    {
      path: 'rule',
      name: 'RuleManagement',
      meta: {
        title: t('routes.demo.system.menu'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/auth/rule/index.vue'),
    },
    {
      path: 'log',
      name: 'LogManagement',
      meta: {
        title: '操作日志',
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/auth/log/index.vue'),
    },
    // {
    //   path: 'changePassword',
    //   name: 'ChangePassword',
    //   meta: {
    //     title: t('routes.demo.system.password'),
    //     ignoreKeepAlive: true,
    //   },
    //   component: () => import('/@/views/auth/password/index.vue'),
    // },
  ],
};

export default auth;
