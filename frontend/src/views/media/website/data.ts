import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { uploadApi } from '/@/api/sys/upload';
import { AllGroupList } from '/@/api/demo/media';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '所属分组',
    dataIndex: 'group.name',
    width: 150,
    defaultHidden: true,
  },
  {
    title: '网站名称',
    dataIndex: 'name',
    width: 200,
    slots: { customRender: 'website' },
  },
  {
    title: 'LOGO',
    dataIndex: 'logo',
    width: 100,
    slots: { customRender: 'logo' },
  },
  {
    title: '链接',
    dataIndex: 'url',
    slots: { customRender: 'link' },
  },
  {
    title: '模块数',
    dataIndex: 'module_num',
    width: 100,
  },
  {
    title: '文章数',
    dataIndex: 'article_num',
    width: 100,
  },
  {
    title: '排序',
    dataIndex: 'weigh',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 200,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'group_id',
    label: '所属分组',
    component: 'ApiSelect',
    componentProps: {
      api: AllGroupList,
      labelField: 'name',
      valueField: 'id',
    },
    colProps: { span: 6 },
  },
  {
    field: 'name',
    label: '网站名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'url',
    label: '链接',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '停用',
          value: 0,
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'group_id',
    label: '所属分组',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: AllGroupList,
      labelField: 'name',
      valueField: 'id',
    },
  },
  {
    field: 'name',
    label: '网站名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'url',
    label: '链接',
    required: true,
    component: 'Input',
  },
  {
    label: 'LOGO',
    field: 'logo',
    component: 'Upload',
    componentProps: {
      api: uploadApi,
      maxSize: 5,
      maxNumber: 1,
      emptyHidePreview: true,
      //accept: ['image/*'],
    },
  },
  {
    field: 'intro',
    label: '简介',
    component: 'InputTextArea',
    componentProps: {
      rows: 3,
    },
  },
  {
    field: 'weigh',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 0,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
      ],
    },
  },
];
