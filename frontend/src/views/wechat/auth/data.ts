import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
    defaultHidden: true,
  },
  {
    title: '公众号名称',
    dataIndex: 'account',
    width: 150,
  },
  {
    title: 'Token',
    dataIndex: 'token',
    width: 150,
  },
  {
    title: 'Cookie',
    dataIndex: 'cookie',
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createtime',
    width: 200,
  },
  {
    title: '更新时间',
    dataIndex: 'updatetime',
    width: 200,
    defaultHidden: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'account',
    label: '公众号名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '启用',
          value: 1,
        },
        {
          label: '停用',
          value: 0,
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'account',
    label: '公众号名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'token',
    label: 'Token',
    required: true,
    component: 'Input',
  },
  {
    field: 'cookie',
    label: 'Cookie',
    required: true,
    component: 'InputTextArea',
    componentProps: {
      rows: 5,
    },
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
      ],
    },
  },
];
