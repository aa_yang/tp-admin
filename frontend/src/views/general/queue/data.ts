import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 300,
    defaultHidden: true,
  },
  {
    title: '任务名称',
    dataIndex: 'queue',
    width: 150,
  },
  {
    title: '队列传输数据',
    dataIndex: 'payload',
    //slots: { customRender: 'json' },
  },
  {
    title: '任务重试次数',
    dataIndex: 'attempts',
    width: 120,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    customRender: ({ record }) => {
      const status = record.status;
      const color = {
        10: 'success',
        20: 'processing',
        30: 'error',
        40: 'warning',
      };
      const text = {
        10: '待执行',
        20: '执行成功',
        30: '执行失败',
        40: '已取消',
      };

      return h(Tag, { color: color[status] }, () => text[status]);
    },
  },
  {
    title: '任务执行时间',
    dataIndex: 'execution_time',
    width: 200,
  },
  {
    title: '任务发布时间',
    dataIndex: 'createtime',
    width: 200,
    defaultHidden: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'queue',
    label: '任务名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        {
          label: '待执行',
          value: 10,
        },
        {
          label: '执行成功',
          value: 20,
        },
        {
          label: '执行失败',
          value: 20,
        },
        {
          label: '已取消',
          value: 20,
        },
      ],
    },
    colProps: { span: 6 },
  },
];

export const logFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'Input',
    componentProps: {
      readonly: true,
    },
  },
  {
    field: 'queue',
    label: '名称',
    component: 'Input',
    componentProps: {
      readonly: true,
    },
  },
  {
    field: 'execution_time',
    label: '时间',
    component: 'Input',
    componentProps: {
      readonly: true,
    },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      disabled: true,
      options: [
        {
          label: '待执行',
          value: 10,
        },
        {
          label: '执行成功',
          value: 20,
        },
        {
          label: '执行失败',
          value: 20,
        },
        {
          label: '已取消',
          value: 20,
        },
      ],
    },
    colProps: { span: 6 },
  },
  {
    field: 'payload',
    label: '数据',
    component: 'InputTextArea',
    componentProps: {
      rows: 5,
    },
    slot: 'json',
  },
];
