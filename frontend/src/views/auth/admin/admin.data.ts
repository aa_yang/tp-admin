import { ajaxGroupList } from '/@/api/demo/auth';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { uploadApi } from '/@/api/sys/upload';

export const columns: BasicColumn[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 100,
  },
  {
    title: '真实姓名',
    dataIndex: 'realname',
    width: 150,
  },
  {
    title: '登录名',
    dataIndex: 'username',
    width: 120,
  },
  {
    title: '头像',
    dataIndex: 'avatar',
    width: 100,
    slots: { customRender: 'imgs' },
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    width: 200,
  },
  {
    title: '最近登陆时间',
    dataIndex: 'login_time',
  },
  {
    title: '最近登陆IP',
    dataIndex: 'login_ip',
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'username',
    label: '登录名',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'realname',
    label: '真实姓名',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'ID',
    component: 'InputNumber',
    defaultValue: 0,
    show: false,
    ifShow: ({ values }) => {
      return values.id !== 0;
    },
  },
  {
    field: 'realname',
    label: '真实姓名',
    component: 'Input',
    required: true,
  },
  {
    field: 'username',
    label: '登录名',
    component: 'Input',
    required: true,
    dynamicDisabled: ({ values }) => {
      return values.id === 1;
    },
  },
  {
    field: 'password',
    label: '密码',
    component: 'InputPassword',
    required: true,
    ifShow: true,
  },
  {
    label: '角色',
    field: 'group',
    component: 'ApiSelect',
    componentProps: {
      api: ajaxGroupList,
      labelField: 'title',
      valueField: 'id',
      mode: 'multiple',
    },
    required: true,
    dynamicDisabled: ({ values }) => {
      return values.id === 1;
    },
  },
  {
    label: '邮箱',
    field: 'email',
    component: 'Input',
    required: true,
  },
  {
    label: '头像',
    field: 'avatar',
    component: 'Upload',
    componentProps: {
      api: uploadApi,
      maxSize: 5,
      maxNumber: 1,
      emptyHidePreview: true,
      //accept: ['image/*'],
    },
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '禁用', value: 0 },
      ],
    },
    dynamicDisabled: ({ values }) => {
      return values.id === 1;
    },
  },
];
