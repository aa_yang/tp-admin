<?php

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'local'),
    // 磁盘列表
    'disks' => [
        'local' => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'public' => [
            // 磁盘类型
            'type' => 'local',
            // 磁盘路径
            'root' => app()->getRootPath() . 'public/storage',
            // 磁盘路径对应的外部URL路径
            'url' => '/storage',
            // 可见性
            'visibility' => 'public',
        ],
        // 更多的磁盘配置信息
        'aliyun' => [
            'type' => 'aliyun',
            'accessKeyId' => 'xxxxxx',
            'accessKeySecret' => 'xxxxxx',
            'bucket' => 'tp-admin',
            'endpoint' => 'https://oss-cn-hangzhou.aliyuncs.com',//自定义域名,可以去掉https
            'isCName' => false,
            'url' => 'https://tp-admin.oss-cn-hangzhou.aliyuncs.com',//不要斜杠结尾，此处为URL地址域名。
        ],
        'qiniu' => [
            'type' => 'qiniu',
            'accessKey' => 'xxxxxx',
            'secretKey' => 'xxxxxx',
            'bucket' => 'tp-admin',
            'url' => 'https://qiniu-cdn.yinhoujie.com',//不要斜杠结尾，此处为URL地址域名。
        ],
        'qcloud' => [
            'type' => 'qcloud',
            'region' => 'ap-nanjing', //bucket 所属区域 英文
            'credentials'=> [
                'appId' => null,
                'secretId' => 'xxxxxx',
                'secretKey' => 'xxxxxx',
                'anonymous' => false,
                'token' => null,
            ],
            'bucket' => 'tp-admin-1256627036',
            'timeout' => 60,
            'connect_timeout' => 60,
            'scheme' => 'https',
            'url' => 'https://tp-admin-1256627036.cos.ap-nanjing.myqcloud.com',//不要斜杠结尾，此处为URL地址域名。
        ]
    ],
];
