<?php

declare (strict_types = 1);

namespace app\common\library\storage\engine;

use app\common\library\storage\Driver;
use OSS\OssClient;
use OSS\Core\OssException;

/**
 * 阿里云存储引擎 (OSS)
 * Class Aliyun
 * @package app\common\library\storage\engine
 */
class Aliyun extends Driver
{
    /**
     * 执行上传
     * @return bool|mixed
     */
    public function upload()
    {
        try {
            $ossClient = new OssClient(
                $this->config['accessKeyId'],
                $this->config['accessKeySecret'],
                $this->config['endpoint'],
                $this->config['isCName']
            );
            $result = $ossClient->uploadFile(
                $this->config['bucket'],
                $this->getSaveFileInfo()['file_path'],
                $this->getRealPath()
            );
        } catch (OssException $e) {
            $this->error = $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * 删除文件
     * @param string $filePath
     * @return bool|mixed
     */
    public function delete(string $filePath)
    {
        try {
            $filePath = ltrim($filePath, '/');
            $ossClient = new OssClient(
                $this->config['accessKeyId'],
                $this->config['accessKeySecret'],
                $this->config['endpoint'],
                $this->config['isCName']
            );
            $ossClient->deleteObject($this->config['bucket'], $filePath);
        } catch (OssException $e) {
            $this->error = $e->getMessage();
            return false;
        }
        return true;
    }

}
