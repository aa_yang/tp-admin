<?php
declare (strict_types=1);

namespace app\common\model;

use app\admin\model\BaseModel;
use think\model\concern\SoftDelete;

class QueueLog extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'deletetime';

    //当前模型对应的数据表名称
    protected $name = 'queue_log';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['id'])) {
                $query->where('id', 'like', '%' . $filter['id'] . '%');
            }
            if (isset($filter['queue'])) {
                $query->where('queue', 'like', '%' . $filter['queue'] . '%');
            }
            if (isset($filter['status'])) {
                $query->where('status', '=', $filter['status']);
            }
        };
        $order = ['execution_time' => 'desc'];
        return self::getPageList($where, $order);
    }

    public function getExecutionTimeAttr($value, $data)
    {
        if ($value) {
            return date('Y-m-d H:i:s', $value);
        } else {
            return '未执行';
        }
    }
}