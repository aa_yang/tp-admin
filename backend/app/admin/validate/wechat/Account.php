<?php
declare (strict_types=1);

namespace app\admin\validate\wechat;

use think\Validate;

class Account extends Validate
{
    protected $rule = [
        'nickname' => 'unique:wechat_account',
    ];

    protected $message = [
        'nickname.unique' => '公众号已存在',
    ];

}