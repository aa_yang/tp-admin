<?php
declare (strict_types=1);

namespace app\admin\validate\media;

use think\Validate;

class Module extends Validate
{
    protected $rule = [
        //'url' => 'unique:media_module',
    ];

    protected $message = [
        //'url.unique' => '链接已存在',
    ];

}