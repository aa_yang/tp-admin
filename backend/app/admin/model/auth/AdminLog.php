<?php
declare (strict_types=1);

namespace app\admin\model\auth;

use app\admin\library\Auth;
use app\admin\model\BaseModel;

class AdminLog extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'admin_log';

    public function getList($filter = []): array
    {
        $auth = Auth::instance();

        $where = function ($query) use ($filter, $auth) {
            if ($auth->isSuperAdmin() == false) {
                $query->where('admin_id', '=', $auth->id);
            }
            if (isset($filter['username'])) {
                $query->where('username', '=', $filter['username']);
            }
            if (isset($filter['title'])) {
                $query->where('title', 'like', '%' . $filter['title'] . '%');
            }
        };
        $order = ['id' => 'desc'];
        return self::getPageList($where, $order);
    }

}