<?php
declare (strict_types=1);

namespace app\admin\model\wechat;

use app\admin\model\BaseModel;
use think\model\concern\SoftDelete;

class Account extends BaseModel
{
    use SoftDelete;

    protected $deleteTime = 'deletetime';

    //当前模型对应的数据表名称
    protected $name = 'wechat_account';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['nickname'])) {
                $query->where('nickname', 'like', '%' . $filter['nickname'] . '%');
            }
            if (isset($filter['alias'])) {
                $query->where('alias', 'like', '%' . $filter['alias'] . '%');
            }
            if (isset($filter['service_type'])) {
                $query->where('service_type', '=', $filter['service_type']);
            }
            if (isset($filter['status'])) {
                $query->where('status', '=', $filter['status']);
            }
        };
        $order = ['weigh' => 'desc', 'id' => 'desc'];
        return self::getPageList($where, $order);
    }

}