<?php
declare (strict_types=1);

namespace app\admin\model\wechat;

use app\admin\model\BaseModel;

class Article extends BaseModel
{
    //当前模型对应的数据表名称
    protected $name = 'wechat_article';

    public function getList($filter = []): array
    {
        $where = function ($query) use ($filter) {
            if (isset($filter['fakeid'])) {
                $query->where('a_fakeid', '=', $filter['fakeid']);
            }
            if (isset($filter['nickname'])) {
                $query->where('a_nickname', 'like', '%' . $filter['nickname'] . '%');
            }
            if (isset($filter['title'])) {
                $query->where('title', 'like', '%' . $filter['title'] . '%');
            }
            if (isset($filter['author'])) {
                $query->where('author', 'like', '%' . $filter['author'] . '%');
            }
            if (isset($filter['copyright_type'])) {
                $query->where('copyright_type', '=', $filter['copyright_type']);
            }
            if (isset($filter['time'])) {
                $startTime = date('Y-m-d', strtotime($filter['time'][0])) . ' 00:00:00';
                $endTime = date('Y-m-d', strtotime($filter['time'][1])) . ' 23:59:59';
                $query->whereTime('updatetime', 'between', [$startTime, $endTime]);
            }
        };
        $order = ['createtime' => 'desc', 'itemidx' => 'asc'];
        return self::getPageList($where, $order);
    }

    public function getPageList($where = [], $order = [], $page = 0, $limit = 0)
    {
        $page = $page ?: request()->param("page", 1, 'intval');
        $limit = $limit ?: request()->param("limit", 10, 'intval');

        $total = self::
        where($where)
            ->count();

        $list = self::withoutField('content,html,word,pdf')
            ->where($where)
            ->order($order)
            ->page($page, $limit)
            ->select();

        return [$total, $list];
    }

    /**
     * 获取已存在的文章-采集
     * @param $account
     * @param $appmsgids
     * @return array
     */
    public function getListByAppmsgid($account, $appmsgids)
    {
        return $this->where('a_appid', '=', $account['appid'])
            ->where('a_fakeid', '=', $account['fakeid'])
            ->where('appmsgid', 'in', $appmsgids)
            ->distinct(true)
            ->column('appmsgid');
    }


    public function saveArticleList($list, $account)
    {
        //公众号文章消息ID
        $appmsgids = array_column($list, 'appmsgid');
        $repeatIds = $this->getListByAppmsgid($account, $appmsgids);
        //文章列表
        $dataList = [];
        foreach ($list as $item) {
            //跳过重复的文章
            if (in_array($item['appmsgid'], $repeatIds)) {
                continue;
            }
            //跳过文字消息
            if ($item['item_show_type'] == 10) {
                continue;
            }
            //原创类型-有搜索条件
            if (isset($item['is_original'])) {
                $item['copyright_type'] = $item['is_original'];
            }
            $item['title'] = strip_tags($item['title']);
            $item['digest'] = preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", " ", strip_tags($item['digest']));

            $dataList[] = [
                'a_appid' => $account['appid'],
                'a_fakeid' => $account['fakeid'],
                'a_nickname' => $account['nickname'],
                'aid' => $item['aid'],
                'appmsgid' => $item['appmsgid'],
                'title' => $item['title'],
                'digest' => $item['digest'],
                'cover' => $item['cover'],
                'link' => $item['link'],
                'itemidx' => $item['itemidx'],
                'copyright_type' => $item['copyright_type'],
                'createtime' => $item['create_time'],
                'updatetime' => $item['update_time'],
            ];
        }
        if ($dataList) {
            $this->saveAll($dataList);
        }
    }

}