<?php
declare (strict_types=1);

namespace app\admin\model;

use think\Model;

class BaseModel extends Model
{
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [];

    /**
     * 简单分页列表
     * @param array $where 搜索条件
     * @param array $order 排序方式
     * @param int $page 页数
     * @param int $limit 每页数量
     * @return array
     */
    public function getPageList($where = [], $order = [], $page = 0, $limit = 0)
    {
        $page = $page ?: request()->param("page", 1, 'intval');
        $limit = $limit ?: request()->param("limit", 10, 'intval');

        $total = self::
            where($where)
            ->count();

        $list = self::
            where($where)
            ->order($order)
            ->page($page, $limit)
            ->select();

        return [$total, $list];
    }
}