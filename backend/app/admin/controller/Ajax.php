<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\common\controller\BaseController;
use think\facade\Db;

class Ajax extends BaseController
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    public function initialize()
    {
        parent::initialize();

    }

    public function getMenuList()
    {
        $menuList = $this->auth->getMenuList();

        return $this->success('请求成功',$menuList);
    }

    public function getUserInfo()
    {
        $userInfo = [
            'userId' => $this->auth->id,
            'username' => $this->auth->username,
            'realName' => $this->auth->realname,
            'avatar' => $this->auth->avatar,
            'token' => $this->auth->getToken(),
            'desc' => '',
            'homePath' => "",
            'roles' => [],
        ];

        return $this->success('请求成功',$userInfo);
    }

    public function getPermCode()
    {
        $roleList = $this->auth->getRoleList();

        return $this->success('请求成功',$roleList);
    }

    public function getGroupList()
    {
        $groupList = $this->auth->getGroupList();

        return $this->success('请求成功',$groupList);
    }
}