<?php
declare (strict_types = 1);

namespace app\admin\controller\auth;

use app\common\controller\BaseController;
use app\admin\model\auth\Rule as RuleModel;

class Rule extends BaseController
{

    /**
     * @var RuleModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new RuleModel;
    }

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isGet()) {

            $filter = $this->request->param('filter/a', []);

            $list = $this->model->getList($filter);

            return $this->success('请求成功',$list);
        }
    }
}