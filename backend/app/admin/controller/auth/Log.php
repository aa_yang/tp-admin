<?php
declare (strict_types = 1);

namespace app\admin\controller\auth;

use app\common\controller\BaseController;
use app\admin\model\auth\AdminLog as LogModel;

class Log extends BaseController
{

    /**
     * @var LogModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new LogModel;
    }

}