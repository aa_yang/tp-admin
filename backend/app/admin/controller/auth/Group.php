<?php
declare (strict_types = 1);

namespace app\admin\controller\auth;

use app\common\controller\BaseController;
use app\admin\model\auth\Group as GroupModel;

class Group extends BaseController
{

    /**
     * @var GroupModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new GroupModel;
    }

}