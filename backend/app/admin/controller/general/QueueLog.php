<?php
declare (strict_types=1);

namespace app\admin\controller\general;

use app\common\controller\BaseController;
use app\common\model\QueueLog as QueueLogModel;

class QueueLog extends BaseController
{

    /**
     * @var QueueLogModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new QueueLogModel;
    }


    /**
     * 删除
     * @param array $ids
     */
    public function del($ids = [])
    {
        if ($this->request->isDelete()) {
            $ids = $this->request->delete();
            if ($ids && is_array($ids)) {
                $pk = $this->model->getPk();
                $list = $this->model->where($pk, 'in', $ids)->select();
                $count = 0;
                foreach ($list as $k => $v) {
                    if($v['status'] == 10) {
                        $v->save(['status' => 40]);
                    }
                    $count += $v->delete();
                }
                if ($count) {
                    return $this->success('删除成功');
                } else {
                    return $this->error(lang('No rows were deleted'));
                }
            }
            return $this->error(lang('Parameter %s can not be empty', 'id'));
        }
    }

}