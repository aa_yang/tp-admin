<?php
declare (strict_types=1);

namespace app\admin\controller\general;

use app\common\controller\BaseController;
use app\admin\model\auth\Admin as AdminModel;

class Profile extends BaseController
{

    /**
     * @var AdminModel
     */
    protected $model;

    public function initialize()
    {
        parent::initialize();

        $this->model = new AdminModel;
    }

    public function changepwd()
    {
        if ($this->request->isPost()) {
            $oldpassword = $this->request->param('oldpassword', '', 'trim');
            $newpassword = $this->request->param('newpassword', '', 'trim');

            if (!$oldpassword || !$newpassword) {
                return $this->error('请输入密码');
            }

            $result = $this->auth->changepwd($newpassword, $oldpassword);
            if ($result !== false) {
                return $this->success('密码修改成功');
            } else {
                return $this->error($this->auth->getError());
            }
        }
    }
}