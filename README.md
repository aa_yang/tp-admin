# TpAdmin

配置
https://blog.csdn.net/donglianyou/article/details/124301837

nginx
```
location / {
  if (!-e $request_filename) {
    rewrite  ^(.*)$  /index.php?s=/$1  last;
  }
}
```
apache
```
<IfModule mod_rewrite.c> #如果mode_rewrite.c模块存在 则执行以下命令
  Options +FollowSymlinks -Multiviews
  RewriteEngine On #开启 rewriteEngine
  # !-d 不是目录或目录不存在
  RewriteCond %{REQUEST_FILENAME} !-d 
  # !-f 不是文件或文件不存在
  RewriteCond %{REQUEST_FILENAME} !-f 

  RewriteRule ^(.*)$ index.php [QSA,PT,L]
  # 参数解释
  # ^(.*)$： 匹配所有的路口映射
  # QSA: （Query String Appending）表示保留参数入get传值？xxx==xx;
  # PT: 把这个URL交给Apache处理；
  # L: 作为最后一条，遇到这条将不再匹配这条之后的规则
</IfModule>

```

#### 介绍
TpAdmin是使用 Thinkphp6 + Vben Admin 的后台管理框架，整合权限管理、公众号文章采集、网站文章采集、云存储、附件管理等为一体的后台管理框架。

### 软件架构
* 后端：`ThinkPHP 6.0`：[https://gitee.com/liu21st/thinkphp](https://gitee.com/liu21st/thinkphp)
* 前端：`VbenAdmin`：[https://github.com/vbenjs/vben-admin-thin-next](https://github.com/vbenjs/vben-admin-thin-next)
* 采集框架：`QueryList`：[https://gitee.com/jae/QueryList](https://gitee.com/jae/QueryList)

### 功能列表
- [x] 权限管理：基于auth的权限管理，对用户分配角色，可进行按钮级别的权限控制
- [x] 微信管理：公众号历史文章采集，最新文章同步
- [x] 信息采集：支持api、html、动态网页的采集
- [x] 操作日志：管理员进行业务操作的所有日志
- [x] 附件管理：系统中上传的所有文件
- [x] 云存储：七牛云，阿里云，腾讯云

### 计划功能
- [ ] 权限管理：按钮级权限控制
- [ ] 信息采集：采集功能优化
- [ ] 信息采集：文章时间转换
- [ ] 信息采集：文章链接转换
- [ ] 系统功能：字典管理
- [ ] 系统功能：系统设置

### 在线演示
* 网址：[http://tp-admin.yinhoujie.com/](http://tp-admin.yinhoujie.com/)
* 账号：ceshi
* 密码：123456

### 项目截图

### 环境推荐
* PHP >= 7.1
* 扩展：rewrite、curl、fileinfo
* MYSQL >= 5.6
* Redis
* Node >= 12
* Yarn 

### 安装说明

* 公众号采集需要有自己的公众号，登陆公众号后台，地址栏能看到公众号的token，F12控制台查看cookie
* 在系统后台 微信管理->cookie管理 中添加公众号的token和cookie，否则采集不生效，cookie有效期为5天

```bash
# 克隆项目
git clone https://gitee.com/wdmzjyhj/tp-admin.git
```

### 安装-后端
```bash
# 进入项目目录
cd backend

# 安装依赖
composer install

# 配置域名
根据本地环境配置域名，例如：www.tp-admin.com

# 数据库
导入数据库，名称：tp-admin，字符集utf8mb4；
修改backend/config/database.php文件中的mysql配置

# queue队列
修改backend/config/queue.php文件中的redis配置

# 队列运行
php think queue:work --queue WechatQueue --tries=3
php think queue:work --queue MediaQueue --tries=3

# 云存储
修改backend/config/filesystem.php文件中对应平台的配置

# 注意
线上环境请设置runtime和public/storage目录权限为777

```

### 安装-前端
```bash
# 进入项目目录
cd frontend

# 安装依赖
yarn install

# 修改配置
开发环境：修改.env.development 中的域名为你本地的域名
例如：www.tp-admin.com
生产环境：修改.env.production 中的域名为你线上的域名
例如：api.tp-admin.com

# 启动服务
yarn serve

# 访问地址
http://localhost:3001

# 登录
账号：admin 
密码：123456

```

```bash
# 项目打包
yarn build

# 项目上传
将dist下的文件上传至服务器
```

### 其他
```
如果对您有帮助，点个Star支持下吧！
```
